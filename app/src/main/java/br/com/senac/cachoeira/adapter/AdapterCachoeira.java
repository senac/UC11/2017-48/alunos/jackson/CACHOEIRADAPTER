package br.com.senac.cachoeira.adapter;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;

public class AdapterCachoeira extends BaseAdapter {

    private List<Cachoeira> lista ;
    private Activity contexto  ;

    public AdapterCachoeira(Activity contexto , List<Cachoeira> lista){
        this.contexto = contexto ;
        this.lista  = lista ;
    }
    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int indice) {
        return this.lista.get(indice);
    }

    @Override
    public long getItemId(int id) {

        int posicao  = 0 ;

        for (int i = 0 ; i < this.lista.size() ; i++){
            if(this.lista.get(i).getId() == id){
                posicao = i ;
                break;
            }
        }

        return posicao;
    }

    @Override
    public View getView(int posicao, View convertView, ViewGroup parent) {

        View view = contexto.getLayoutInflater()
                .inflate(R.layout.cachoeira_lista , parent , false) ;


        ImageView imageView = view.findViewById(R.id.foto) ;
        TextView textViewNome = view.findViewById(R.id.nome) ;
        RatingBar ratingBarClassificacao = view.findViewById(R.id.rtClassificacao) ;

        Cachoeira cachoeira = this.lista.get(posicao) ;

        imageView.setImageBitmap(cachoeira.getImagem());
        textViewNome.setText(cachoeira.getNome());
        ratingBarClassificacao.setRating(cachoeira.getClassificacao());

        return view;
    }





















}
