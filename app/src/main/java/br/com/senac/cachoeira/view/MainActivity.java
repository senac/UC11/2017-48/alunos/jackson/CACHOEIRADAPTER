package br.com.senac.cachoeira.view;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.EGLExt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.CachoeiraDao;
import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.adapter.AdapterCachoeira;
import br.com.senac.cachoeira.model.Cachoeira;

public class MainActivity extends AppCompatActivity {

    public static final  int  REQUEST_NOVO = 1 ;

    private static Bitmap imagem   ;

    public static Bitmap getImagem(){
        return imagem;
    }


    public static final String CACHOEIRA = "cachoeira" ;
    public static final String  IMAGEM =  "imagem" ;


    private ListView listView  ;

    private ArrayAdapter<Cachoeira> adapter  ;

    private Cachoeira cachoeiraSelecionada ;

    private List<Cachoeira> lista ;
    private CachoeiraDao dao ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.ListaCachoeiras) ;

        /* inicializando cachoeiras padrao .....
        Cachoeira rioDoMeio = new Cachoeira();
        rioDoMeio.setId(1);
        rioDoMeio.setNome(getResources().getString(R.string.Rio_do_Meio));
        rioDoMeio.setInformocoes(getResources().getString(R.string.Rio_do_Meio_info));
        rioDoMeio.setClassificacao(Float.parseFloat(getResources().getString(R.string.Rio_do_Meio_classificacao)));
        rioDoMeio.setImagem(BitmapFactory.decodeResource(getResources() , R.drawable.rio_do_meio));
        rioDoMeio.setEmail("rio@cachoeira.com.br");
        rioDoMeio.setEndereco("Comunidade do Mangaraí, Santa Leopoldina - ES, 29640-000");
        rioDoMeio.setSite("http://www.santaleopoldina.es.gov.br/VerPontoTuristico.aspx?no=4");
        rioDoMeio.setTelefone("(27) 99927-7517");

        Cachoeira matilde = new Cachoeira();
        matilde.setId(2);
        matilde.setNome(getResources().getString(R.string.Matilde));
        matilde.setInformocoes(getResources().getString(R.string.Matilde_info));
        matilde.setClassificacao(Float.parseFloat(getResources().getString(R.string.Matilde_classificacao)));
        matilde.setImagem(BitmapFactory.decodeResource(getResources(), R.drawable.matilde));
        matilde.setEmail("matilde@cachoeira.com.br");
        rioDoMeio.setEndereco("R. Orlindo Donadello, s/n, Alfredo Chaves - ES, 29240-000");
        rioDoMeio.setSite("http://cachoeiradematilde.business.site");
        rioDoMeio.setTelefone("(27) 99991-5631");


        Cachoeira fumaca = new Cachoeira();
        fumaca.setId(3);
        fumaca.setNome(getResources().getString(R.string.Cachoeira_da_Fumaca));
        fumaca.setInformocoes(getResources().getString(R.string.Cachoeira_da_Fumaca_info));
        fumaca.setClassificacao(Float.parseFloat(getResources().getString(R.string.Cachoeira_da_Fumaca_classificacao)));
        fumaca.setImagem(BitmapFactory.decodeResource(getResources() , R.drawable.cachoeira_da_fumaca));
        fumaca.setEmail("fumaca@cachoeira.com.br");
        rioDoMeio.setEndereco("Alegre - ES, 29500-000");
        rioDoMeio.setSite("https://iema.es.gov.br/PECF");
        rioDoMeio.setTelefone("(27) 3636-2570");

        lista.add(rioDoMeio);
        lista.add(matilde);
        lista.add(fumaca);
*/

        /* pegar elementos da view */







        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                cachoeiraSelecionada  = (Cachoeira)adapter.getItemAtPosition(posicao) ;

                Intent intent = new Intent(MainActivity.this , DetalheActivity.class) ;
                intent.putExtra(CACHOEIRA , cachoeiraSelecionada);

                imagem = cachoeiraSelecionada.getImagem() ;


                /* compactar a imagem para ser passada na intencao
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                cachoeira.getImagem().compress(Bitmap.CompressFormat.PNG , 100 , stream) ;
                byte[] bytes = stream.toByteArray() ;


                intent.putExtra(IMAGEM , bytes) ;

                */


                startActivity(intent);


            }
        });




        registerForContextMenu(listView);


    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo ;
        cachoeiraSelecionada = (Cachoeira) adapter.getItem(info.position);


        MenuItem itemMenuSite =  menu.add("Visitar site");
        Intent intentSite = new Intent(Intent.ACTION_DEFAULT) ;
        String site = "www.google.com";

        //www.google.com.br

        if(!site.startsWith("http://")){
            site = "http://" + site ;
        }

        intentSite.setData(Uri.parse(site)) ;
        itemMenuSite.setIntent(intentSite) ;







        menu.add("Enviar E-mail") ;
        menu.add("Ligar") ;

        menu.add("Como Chegar") ;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;
    }

    public void novo(MenuItem item){

        Intent intent = new Intent(this , NovoActivity.class);
        startActivityForResult(intent , REQUEST_NOVO);


    }

    @Override
    protected void onResume() {
        super.onResume();



        dao = new CachoeiraDao(this);
        lista = dao.getLista();
        dao.close();

        adapter = new ArrayAdapter<Cachoeira>(this, android.R.layout.simple_list_item_1, lista);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_NOVO){

            switch (resultCode){
                case RESULT_OK :
                    Cachoeira cachoeira = (Cachoeira) data.getSerializableExtra(MainActivity.CACHOEIRA);
                    cachoeira.setImagem(NovoActivity.getImagem());
                    lista.add(cachoeira);
                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_CANCELED :
                        Toast.makeText(this, "Cancelou" , Toast.LENGTH_LONG).show();
                        break;

            }

        }



    }

    public void sobre (MenuItem item){
        Intent intent = new Intent(this , SobreActivity.class) ;
        startActivity(intent);


    }





















}
