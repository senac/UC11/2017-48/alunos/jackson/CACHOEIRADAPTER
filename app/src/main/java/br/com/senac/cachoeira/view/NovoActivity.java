package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.CachoeiraDao;
import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;

public class NovoActivity extends AppCompatActivity {

    public static final int REQUEST_IMAGE_CAPTURE = 1;

    private static Bitmap imagem;

    public static Bitmap getImagem() {
        return imagem;
    }


    private ImageView imageView;
    private EditText editTextNome;
    private EditText editTextInformacoes;
    private EditText editTextEmail;
    private EditText editTextTelefone;
    private EditText editTextEndereco;
    private EditText editTextSite;
    private RatingBar ratingBarClassificacao;
    private Button buttonSalvar;

    private CachoeiraDao dao;
    private Cachoeira cachoeira;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);

        imageView = findViewById(R.id.foto);
        editTextNome = findViewById(R.id.nome);
        editTextInformacoes = findViewById(R.id.informacoes);
        editTextEmail = findViewById(R.id.email);
        editTextTelefone = findViewById(R.id.telefone);
        editTextEndereco = findViewById(R.id.endereco);
        editTextSite = findViewById(R.id.site);
        ratingBarClassificacao = findViewById(R.id.rtClassificacao);
        buttonSalvar = findViewById(R.id.btnSalvar);

    }


    public void capturarImagem(View view) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imagem = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imagem);

        }


    }

    private boolean IsPreenchido(String valeu) {
        return (valeu != null && !valeu.isEmpty());
    }

    public void valiarDados() throws Exception {

        List<String> listaCamposRequeridos = new ArrayList<>();

        if (!IsPreenchido(cachoeira.getNome())) {
            listaCamposRequeridos.add("Nome");
            editTextNome.setBackgroundColor(getResources().getColor(R.color.vermelho));
        }

        if (!IsPreenchido(cachoeira.getInformocoes())) {
            listaCamposRequeridos.add("Informações");
        }

        if (!IsPreenchido(cachoeira.getEmail())) {
            listaCamposRequeridos.add("E-mail");
        }

        if (!IsPreenchido(cachoeira.getTelefone())) {
            listaCamposRequeridos.add("Telefone");
        }


        if (!IsPreenchido(cachoeira.getEndereco())) {
            listaCamposRequeridos.add("Endereço");
        }


        if (!IsPreenchido(cachoeira.getSite())) {
            listaCamposRequeridos.add("Site");

        }

        if (listaCamposRequeridos.size() > 0) {
            throw new Exception("Campos requerido(s)" + listaCamposRequeridos.toString());
        }


    }


    public void salvar(View view) {


        try {



            String nome = editTextNome.getText().toString();
            String informacao = editTextInformacoes.getText().toString();
            String email = editTextEmail.getText().toString();
            String telefone = editTextTelefone.getText().toString();
            String endereco = editTextEndereco.getText().toString();
            String site = editTextSite.getText().toString();

            float classificacao = ratingBarClassificacao.getRating();
            Bitmap bmp = imageView.getDrawingCache();


            cachoeira = new Cachoeira();
            cachoeira.setNome(nome);
            cachoeira.setInformocoes(informacao);
            cachoeira.setClassificacao(classificacao);
            cachoeira.setImagem(bmp);
            cachoeira.setEmail(email);
            cachoeira.setTelefone(telefone);
            cachoeira.setEndereco(endereco);
            cachoeira.setSite(site);


            this.valiarDados();


            dao = new CachoeiraDao(NovoActivity.this);
            dao.salvar(cachoeira);
            dao.close();

            Toast.makeText(NovoActivity.this ,
                    "Salvo com sucesso." ,
                    Toast.LENGTH_LONG).show();

            // voltar main activity
           /*
            Intent intent = new Intent();
            intent.putExtra(MainActivity.CACHOEIRA, cachoeira);
            setResult(RESULT_OK, intent);

            */

            finish();

        } catch (Exception ex) {

            Toast.makeText(NovoActivity.this ,
                    "Erro ao salvar." + ex.getMessage() ,
                    Toast.LENGTH_LONG).show();

        }


    }


}
