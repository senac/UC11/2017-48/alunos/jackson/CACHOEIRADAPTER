package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.model.Cachoeira;

public class DetalheActivity extends AppCompatActivity {

    private Cachoeira cachoeira ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent = getIntent() ;


        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA) ;

        if(cachoeira != null){

            /* descodificar objeto
            Bundle bundle = intent.getExtras() ;
            byte[] bytes =  bundle.getByteArray(MainActivity.IMAGEM) ;
            Bitmap imagem = BitmapFactory.decodeByteArray(bytes , 0 , bytes.length) ;



            if(imagem != null){
                cachoeira.setImagem(imagem);
            }else{
                cachoeira.setImagem(BitmapFactory.decodeResource(getResources() , R.drawable.no_image));
            }

            */


            TextView textViewNome = findViewById(R.id.titulo) ;
            TextView textViewInformacoes = findViewById(R.id.informacoes);
            RatingBar ratingBarClassificacao = findViewById(R.id.rtClassificacao) ;
            ImageView imageView = findViewById(R.id.foto) ;




            /*
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.cachoeira_da_fumaca));


            Bitmap imagem = BitmapFactory.decodeResource(getResources() , R.drawable.cachoeira_da_fumaca) ;
            imageView.setImageBitmap(imagem);
              */

            Bitmap imagem = MainActivity.getImagem()  ;

            cachoeira.setImagem(imagem);



            textViewNome.setText(cachoeira.getNome());
            textViewInformacoes.setText(cachoeira.getInformocoes());
            ratingBarClassificacao.setRating(cachoeira.getClassificacao());
            imageView.setImageBitmap(cachoeira.getImagem());


        }































    }
}
