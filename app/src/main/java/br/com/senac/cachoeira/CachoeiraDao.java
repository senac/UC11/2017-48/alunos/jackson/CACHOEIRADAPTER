package br.com.senac.cachoeira;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.model.Cachoeira;

/**
 * Created by sala304b on 28/02/2018.
 */

public class CachoeiraDao extends SQLiteOpenHelper {

    private static final String DATABASE = "SQLite" ;
    private static final int VERSAO = 1 ;


    public CachoeiraDao(Context context) {
        super(context, DATABASE , null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String ddl =  "CREATE TABLE Cachoeiras " +
                "( id PRIMARY KEY , " +
                "nome TEXT NOT NULL , " +
                "informocoes TEXT  ,  " +
                "imagem TEXT  , " +
                "classificacao TEXT  , " +
                "email TEXT  , " +
                "telefone TEXT  , " +
                "endereco TEXT  , " +
                "site TEXT ) ; " ;

        db.execSQL(ddl);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        String ddl  = "DROP TABLE IF EXISTS Cachoeiras ;" ;
        db.execSQL(ddl);
        this.onCreate(db);
    }

    public void salvar(Cachoeira cachoeira) {
        ContentValues values = new ContentValues() ;
        values.put("nome" , cachoeira.getNome());
        values.put("informocoes" , cachoeira.getNome());
        values.put("imagem" , cachoeira.getNome());
        values.put("classificacao" , cachoeira.getNome());
        values.put("email" , cachoeira.getNome());
        values.put("telefone" , cachoeira.getNome());
        values.put("endereco" , cachoeira.getNome());
        values.put("site" , cachoeira.getNome());

        getWritableDatabase().insert(
                "Cachoeiras" ,
                null ,
                values) ;
    }

    public List<Cachoeira> getLista() {
        List<Cachoeira> lista = new ArrayList<>();
        String colunas[] = {"id" , "nome" , "informocoes" , "imagem" , "classificacao" , "email" , "telefone" , "endereco" , "site"} ;

        Cursor cursor =  getWritableDatabase().query(
                "Cachoeiras",
                colunas,
                null,
                null,
                null,
                null,
                null);

        while(cursor.moveToNext()) {
            Cachoeira cachoeira = new Cachoeira();
            cachoeira.setId(cursor.getInt(0));
            cachoeira.setNome(cursor.getString(1));
            cachoeira.setInformocoes(cursor.getString(2));
            cachoeira.setClassificacao(cursor.getFloat(4));
            cachoeira.setEmail(cursor.getString(5));
            cachoeira.setTelefone(cursor.getString(6));
            cachoeira.setEndereco(cursor.getString(7));
            cachoeira.setSite(cursor.getString(8));

            lista.add(cachoeira);
        }

        return lista ;

    }

}
